//
//  UITextFieldExtension.swift
//  GitHubUsers
//
//  Created by Olcay Ertaş on 20.12.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit
import Combine


extension UITextField {
    var textPublisher: AnyPublisher<String, Never> {
        NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: self)
            .compactMap { $0.object as? UITextField }
            .map { $0.text ?? "" }
            .eraseToAnyPublisher()
    }
}
