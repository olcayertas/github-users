//
//  UIImageExtension.swift
//  GitHubUsers
//
//  Created by Olcay Ertaş on 20.12.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit
import CoreImage

extension UIImage {
    
    func colorInverted() -> UIImage {
        guard let ciImage = CIImage(image: self) else {
            print("UIImage: Failed to get CIImage!")
            return self
        }
        guard let filter = CIFilter(name: "CIColorInvert") else {
            print("UIImage: Failed to get filter!")
            return self
        }
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        guard let image = filter.outputImage else {
            print("UIImage: Failed to get output image!")
            return self
        }
        return UIImage(ciImage: image)
    }
}
