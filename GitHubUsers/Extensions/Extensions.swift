//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit
import Combine

extension UIStoryboard {

    static func viewController<T>(_ identifier: String) -> T? where T: UIViewController{
        let sb = UIStoryboard(name: "Main", bundle: .main)
        let identifier = String(describing: T.self)
        return sb.instantiateViewController(withIdentifier: identifier) as? T
    }
}

extension CodingUserInfoKey {
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")!
}

extension String {

    func print() {
        Swift.print(self)
    }
}

extension UITextView {
    var textPublisher: AnyPublisher<String, Never> {
        NotificationCenter.default
                .publisher(for: UITextView.textDidChangeNotification, object: self)
                .compactMap { $0.object as? UITextView }
                .map { $0.text ?? "" }
                .eraseToAnyPublisher()
    }
}

