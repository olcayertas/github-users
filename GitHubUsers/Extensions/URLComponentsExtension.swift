//
//  URLComponentsExtension.swift
//  GitHubUsers
//
//  Created by Olcay Ertaş on 20.12.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import Foundation


extension URLComponents {
    
    init(scheme: String, host: String) {
        self.init()
        self.scheme = scheme
        self.host = host
    }
}
