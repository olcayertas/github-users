//
//  UIViewExtensions.swift
//  GitHubUsers
//
//  Created by Olcay Ertaş on 20.12.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import UIKit


extension UIView {
    
    func centerInParent(_ superView: UIView) {
        centerXAnchor.constraint(equalTo: superView.layoutMarginsGuide.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: superView.layoutMarginsGuide.centerYAnchor).isActive = true
    }
    
    func anchors(equalTo superView: UIView) {
        topAnchor.constraint(equalTo: superView.layoutMarginsGuide.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.layoutMarginsGuide.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: superView.layoutMarginsGuide.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: superView.layoutMarginsGuide.trailingAnchor).isActive = true
    }
    
    func anchors(equalTo superView: UIView, padding: CGFloat) {
        topAnchor.constraint(equalTo: superView.layoutMarginsGuide.topAnchor, constant: padding).isActive = true
        bottomAnchor.constraint(equalTo: superView.layoutMarginsGuide.bottomAnchor, constant: -padding).isActive = true
        leadingAnchor.constraint(equalTo: superView.layoutMarginsGuide.leadingAnchor, constant: padding).isActive = true
        trailingAnchor.constraint(equalTo: superView.layoutMarginsGuide.trailingAnchor, constant: padding).isActive = true
    }
}
