//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit
import Combine


class GitHubUserDetailViewController: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var followingsLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    private let viewModel = GitHubUserDetailViewModel()
    private var bindings = Set<AnyCancellable>()
    private var note: String?
    private var _note: String?

    var _user: GitHubUser?
    var user: GitHubUser? {
        didSet {
            _user = user
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setBindings()
        loadData()
    }

    private func setupLayout() {
        navigationController?.navigationBar.isHidden = false
        noteTextView.layer.borderWidth = 1.0
        noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        noteTextView.layer.cornerRadius = 8
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification:NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 60
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }

    @IBAction private func onSave() {
        user?.note = note
        _note = note
        try? user?.managedObjectContext?.save()
        saveButton.isEnabled = false
    }

    private func onNoteChanged(_ note: String) {
        //note.print()
        self.note = note
        saveButton.isEnabled = note != _note
    }

    private func setData() {
        aboutTextView.text = user?.description
        followersLabel.text = "followers: \(user?.followers ?? 0)"
        followingsLabel.text = "following: \(user?.following ?? 0)"
        noteTextView.text = user?.note
        note = user?.note
        _note = user?.note
        saveButton.isEnabled = false
        stackView.isHidden = false
    }

    func loadData() {
        if let id = user?.id {
            showIndicator()
            viewModel.loadAvatar(id: Int(id))
        }
    }

    private func showIndicator() {
        indicator.isHidden = false
        indicator.startAnimating()
    }

    private func hideIndicator() {
        indicator.isHidden = true
        indicator.startAnimating()
    }

    func refresh(_ user: GitHubUser) {
        self.user?.following = user.following
        self.user?.followers = user.followers
        self.user?.bio = user.bio
        self.user?.blog = user.blog
        self.user?.company = user.company
        self.user?.twitterUsername = user.twitterUsername
        self.user?.name = user.name
        self.user?.location = user.location
        self.user?.email = user.email
    }

    private func setBindings() {

        noteTextView.textPublisher.sink { [weak self] note in
            self?.onNoteChanged(note)
        }.store(in: &bindings)

        viewModel.$avatarData.sink { [weak self] data in
            self?.hideIndicator()
            if let data = data {
                self?.avatarImageView.image = UIImage(data: data)
            }
            if let login = self?.user?.login {
                self?.showIndicator()
                self?.viewModel.getUserDetails(userName: login)
            }
        }.store(in: &bindings)

        viewModel.$user.sink { [weak self] user in
            self?.hideIndicator()
            if let user = user {
                self?.refresh(user)
            }
            self?.setData()
        }.store(in: &bindings)
    }
}
