//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import CoreData


class GitHubUserDetailViewModel: ObservableObject {

    private let avatarClient = AvatarClient()
    private let apiClient = GitHubUserDetailClient()
    private var bindings = Set<AnyCancellable>()

    @Published var avatarData: Data?
    @Published var user: GitHubUser?

    init() {
        apiClient.decoder.userInfo[.managedObjectContext] = GHUContainer.instance.backgroundContext
    }

    func loadAvatar(id: Int) {
        avatarClient.avatar(id: id)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        return
                            //print("Finished getting user avatar.")
                    case .failure(let error):
                        print("Failed to get user avatar: \(error.localizedDescription)")
                    }
                }, receiveValue: { [weak self] data in
                    self?.avatarData = data
                }).store(in: &bindings)
    }

    func getUserDetails(userName: String) {
        apiClient.getDetail(username: userName)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        return
                            //print("Finished getting user detail.")
                    case .failure(let error):
                        print("Failed to get user detail: \(error.localizedDescription)")
                    }
                }, receiveValue: { [weak self] user in
                    self?.user = user
                }).store(in: &bindings)
    }
}
