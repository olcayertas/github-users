//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import CoreData

class GitHubUserListClient: BaseApiClient {

    private let host = "api.github.com"

    override init() {
        super.init()
    }

    init(context: NSManagedObjectContext) {
        super.init()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext] = context
    }
}

extension GitHubUserListClient: GitHubClientProtocol {

    private func url(since: Int) -> URL? {
        var components = URLComponents(scheme: scheme, host: host)
        components.path = "/users"
        components.queryItems = [
            URLQueryItem(name: "since", value: "\(since)")
        ]
        return components.url
    }

    func users(since: Int) -> AnyPublisher<[GitHubUser], Error> {
        guard let url = url(since: since) else {
            return Fail(error: ApiClientError.failedToBuildUrlFromComponents).eraseToAnyPublisher()
        }
        return publisherFor(url: url)
                .decode(type: [GitHubUser].self, decoder: decoder)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
    }
}

