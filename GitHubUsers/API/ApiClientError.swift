//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

public enum ApiClientError: Error, LocalizedError {

    case serverIsBusy
    case rateLimited
    case notFound
    case badRequest
    case failedToBuildUrlFromComponents
    case failedToCreateUrl(String)
    case failedToDecodeResponse
    case networkError(URLError)
    case apiError(String)
    case responseError
    case unauthorized
    case unknown(Int)

    public var errorDescription: String? {
        switch self {
        case .badRequest:
            return "Bad request!"
        case .notFound:
            return "Not found!"
        case .unknown:
            return "Unknown error!"
        case let .apiError(error):
            return error
        case .failedToBuildUrlFromComponents:
            return "Did you set base URL?"
        case let .failedToCreateUrl(endpoint):
            return "Invalid URL: \(endpoint)"
        case .failedToDecodeResponse:
            return "Failed to decode response!"
        case .responseError:
            return "Failed to get response!"
        case let .networkError(error):
            return error.localizedDescription
        case .unauthorized:
            return "Unauthorized!"
        case .rateLimited:
            return "Too Many Requests!"
        case .serverIsBusy:
            return "Server is busy!"
        }
    }
}