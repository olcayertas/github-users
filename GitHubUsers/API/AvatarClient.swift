//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine

class AvatarClient: BaseApiClient {

    private let host = "avatars2.githubusercontent.com"

    private func imageUrl(id: Int) -> URL? {
        var components = URLComponents(scheme: scheme, host: host)
        components.path = "/u/\(id)"
        components.queryItems = [
            URLQueryItem(name: "v", value: "4"),
        ]
        return components.url
    }

    func avatar(id: Int) -> AnyPublisher<Data, Error> {
        guard let url = imageUrl(id: id) else {
            return Fail(error: ApiClientError.failedToBuildUrlFromComponents).eraseToAnyPublisher()
        }
        return publisherFor(url: url)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
    }
}
