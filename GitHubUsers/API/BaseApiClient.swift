//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine

class BaseApiClient {

    internal let scheme = "https"
    internal var decoder = GitHubUserDecoder()

    private let reachability = ReachabilityService()
    private var currentRequestCancellable: AnyCancellable?
    private var connectivityCancellable: AnyCancellable?
    private var publisher: AnyPublisher<Data, Error>?

    init() {
        URLSessionConfiguration.default.waitsForConnectivity = true
    }

    internal func publisherFor(url: URL) -> AnyPublisher<Data, Error> {

        func onConnectivityChanged(_ isConnected: Bool, _ promise: @escaping (Result<Data, Error>) -> ()) {
            //print("BaseApiClient: Connectivity status: ", isConnected)
            if isConnected == true {
                currentRequestCancellable = publisher?.sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        break
                    case .failure(let error):
                        promise(.failure(error))
                    }
                }, receiveValue: { data in
                    promise(.success(data))
                })
            }
        }

        func handlePromise(_ promise: @escaping (Result<Data, Error>) -> ()) {
            //print("BaseApiClient: handlePromise")
            connectivityCancellable?.cancel()
            connectivityCancellable = reachability.$isNetworkAvailable.sink { isConnected in
                onConnectivityChanged(isConnected ?? false, promise)
            }
        }

        publisher = URLSession.shared.dataTaskPublisher(for: url).share().tryMap { data, response in
            guard let httpResponse = response as? HTTPURLResponse else {
                throw ApiClientError.responseError
            }
            //print("\(httpResponse.statusCode) \(response.url?.absoluteString ?? "?")")
            guard 200..<300 ~= httpResponse.statusCode else {
                switch httpResponse.statusCode {
                case 400:
                    throw ApiClientError.badRequest
                case 401:
                    throw ApiClientError.unauthorized
                case 404:
                    throw ApiClientError.notFound
                case 429:
                    throw ApiClientError.notFound
                case 500:
                    throw ApiClientError.responseError
                case 503:
                    throw ApiClientError.serverIsBusy
                default:
                    throw ApiClientError.unknown(httpResponse.statusCode)
                }
            }
            return data
        }.eraseToAnyPublisher()

        return Future<Data, Error> { promise in
            handlePromise(promise)
        }.eraseToAnyPublisher()
    }
}
