//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine


class GitHubUserDetailClient: BaseApiClient {
    private let host = "api.github.com"
}

extension GitHubUserDetailClient: GitHubUserDetailClientProtocol {

    private func url(username: String) -> URL? {
        var components = URLComponents(scheme: scheme, host: host)
        components.path = "/users/\(username)"
        return components.url
    }

    func getDetail(username: String) -> AnyPublisher<GitHubUser, Error> {
        guard let url = url(username: username) else {
            return Fail(error: ApiClientError.failedToBuildUrlFromComponents).eraseToAnyPublisher()
        }
        print("GET \(url.absoluteString)")
        return publisherFor(url: url)
                .decode(type: GitHubUser.self, decoder: decoder)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
    }
}