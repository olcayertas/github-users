//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine

protocol GitHubClientProtocol {
    func users(since: Int) -> AnyPublisher<[GitHubUser], Error>
}

protocol GitHubUserDetailClientProtocol {
    func getDetail(username: String) -> AnyPublisher<GitHubUser, Error>
}