//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import CoreData

public enum GitHubUserDecodeError: Error, LocalizedError {
    case failedToGetContext
}

@objc(GitHubUser)
class GitHubUser: NSManagedObject, Codable, Identifiable {
    
    public static func fetchRequest(_ since: Int, _ limit: Int) -> GitHubUserRequest {
        let request = NSFetchRequest<GitHubUser>(entityName: "GitHubUser")
        //Sort by ID
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        //Get users after since value
        request.predicate = NSPredicate(format: "id > %d", since)
        //Paging
        request.fetchLimit = limit
        return request
    }
    
    public static func fetchRequest() -> GitHubUserRequest {
        NSFetchRequest<GitHubUser>(entityName: "GitHubUser")
    }

    static func ==(lhs: GitHubUser, rhs: GitHubUser) -> Bool {
        lhs.login == rhs.login
    }

    @NSManaged public var avatarUrl: String?
    @NSManaged public var bio: String?
    @NSManaged public var blog: String?
    @NSManaged public var company: String?
    @NSManaged public var createdAt: String?
    @NSManaged public var email: String?
    @NSManaged public var eventsUrl: String?
    @NSManaged public var followers: Int64
    @NSManaged public var followersUrl: String?
    @NSManaged public var following: Int64
    @NSManaged public var followingUrl: String?
    @NSManaged public var gistsUrl: String?
    @NSManaged public var gravatarId: String?
    @NSManaged public var hireable: Bool
    @NSManaged public var htmlUrl: String?
    @NSManaged public var id: Int64
    @NSManaged public var location: String?
    @NSManaged public var login: String?
    @NSManaged public var name: String?
    @NSManaged public var nodeId: String?
    @NSManaged public var note: String?
    @NSManaged public var organizationsUrl: String?
    @NSManaged public var publicGists: Int64
    @NSManaged public var publicRepos: Int64
    @NSManaged public var receivedEventsUrl: String?
    @NSManaged public var reposUrl: String?
    @NSManaged public var siteAdmin: Bool
    @NSManaged public var starredUrl: String?
    @NSManaged public var subscriptionsUrl: String?
    @NSManaged public var twitterUsername: String?
    @NSManaged public var type: String?
    @NSManaged public var updatedAt: String?
    @NSManaged public var url: String?
    @NSManaged public var hasBeenSeen: Bool

    enum CodingKeys: CodingKey {
        case login
        case id
        case nodeId
        case avatarUrl
        case gravatarId
        case url
        case htmlUrl
        case followersUrl
        case followingUrl
        case gistsUrl
        case starredUrl
        case subscriptionsUrl
        case organizationsUrl
        case reposUrl
        case eventsUrl
        case receivedEventsUrl
        case type
        case siteAdmin
        case name
        case company
        case blog
        case location
        case email
        case hireable
        case bio
        case twitterUsername
        case publicRepos
        case publicGists
        case followers
        case following
        case createdAt
        case updatedAt
        case note
        case hasBeenSeen
    }

    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    required convenience init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[.managedObjectContext] as? NSManagedObjectContext else {
            throw GitHubUserDecodeError.failedToGetContext
        }
        self.init(context: context)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        avatarUrl = (try? container.decode(String.self, forKey: .avatarUrl)) ?? ""
        bio = (try? container.decode(String.self, forKey: .bio)) ?? ""
        blog = (try? container.decode(String.self, forKey: .blog)) ?? ""
        company = (try? container.decode(String.self, forKey: .company)) ?? ""
        createdAt = (try? container.decode(String.self, forKey: .createdAt)) ?? ""
        email = (try? container.decode(String.self, forKey: .email)) ?? ""
        eventsUrl = (try? container.decode(String.self, forKey: .eventsUrl)) ?? ""
        followers = (try? container.decode(Int64.self, forKey: .followers)) ?? 0
        followersUrl = (try? container.decode(String.self, forKey: .followersUrl)) ?? ""
        following = (try? container.decode(Int64.self, forKey: .following)) ?? 0
        followingUrl = (try? container.decode(String.self, forKey: .followingUrl)) ?? ""
        gistsUrl = (try? container.decode(String.self, forKey: .gistsUrl)) ?? ""
        gravatarId = (try? container.decode(String.self, forKey: .gravatarId)) ?? ""
        hireable = (try? container.decode(Bool.self, forKey: .hireable)) ?? false
        htmlUrl = (try? container.decode(String.self, forKey: .htmlUrl)) ?? ""
        id = (try? container.decode(Int64.self, forKey: .id))!
        location = (try? container.decode(String.self, forKey: .location)) ?? ""
        login = (try? container.decode(String.self, forKey: .login)) ?? ""
        name = (try? container.decode(String.self, forKey: .name)) ?? ""
        nodeId = (try? container.decode(String.self, forKey: .nodeId)) ?? ""
        note = (try? container.decode(String.self, forKey: .note)) ?? ""
        organizationsUrl = (try? container.decode(String.self, forKey: .organizationsUrl)) ?? ""
        publicGists = (try? container.decode(Int64.self, forKey: .publicGists)) ?? 0
        publicRepos = (try? container.decode(Int64.self, forKey: .publicRepos)) ?? 0
        receivedEventsUrl = (try? container.decode(String.self, forKey: .receivedEventsUrl)) ?? ""
        reposUrl = (try? container.decode(String.self, forKey: .reposUrl)) ?? ""
        siteAdmin = (try? container.decode(Bool.self, forKey: .siteAdmin)) ?? false
        starredUrl = (try? container.decode(String.self, forKey: .starredUrl)) ?? ""
        subscriptionsUrl = (try? container.decode(String.self, forKey: .subscriptionsUrl)) ?? ""
        twitterUsername = (try? container.decode(String.self, forKey: .twitterUsername)) ?? ""
        type = (try? container.decode(String.self, forKey: .type)) ?? ""
        updatedAt = (try? container.decode(String.self, forKey: .updatedAt)) ?? ""
        url = (try? container.decode(String.self, forKey: .url)) ?? ""
        hasBeenSeen = (try? container.decode(Bool.self, forKey: .hasBeenSeen)) ?? false
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(avatarUrl, forKey: .avatarUrl)
        try container.encode(bio, forKey: .bio)
        try container.encode(blog, forKey: .blog)
        try container.encode(company, forKey: .company)
        try container.encode(createdAt, forKey: .createdAt)
        try container.encode(email, forKey: .email)
        try container.encode(eventsUrl, forKey: .eventsUrl)
        try container.encode(followers, forKey: .followers)
        try container.encode(followersUrl, forKey: .followersUrl)
        try container.encode(following, forKey: .following)
        try container.encode(followingUrl, forKey: .followingUrl)
        try container.encode(gistsUrl, forKey: .gistsUrl)
        try container.encode(gravatarId, forKey: .gravatarId)
        try container.encode(hireable, forKey: .hireable)
        try container.encode(htmlUrl, forKey: .htmlUrl)
        try container.encode(id, forKey: .id)
        try container.encode(location, forKey: .location)
        try container.encode(login, forKey: .login)
        try container.encode(name, forKey: .name)
        try container.encode(nodeId, forKey: .nodeId)
        try container.encode(note, forKey: .note)
        try container.encode(organizationsUrl, forKey: .organizationsUrl)
        try container.encode(publicGists, forKey: .publicGists)
        try container.encode(publicRepos, forKey: .publicRepos)
        try container.encode(receivedEventsUrl, forKey: .receivedEventsUrl)
        try container.encode(reposUrl, forKey: .reposUrl)
        try container.encode(siteAdmin, forKey: .siteAdmin)
        try container.encode(starredUrl, forKey: .starredUrl)
        try container.encode(subscriptionsUrl, forKey: .subscriptionsUrl)
        try container.encode(twitterUsername, forKey: .twitterUsername)
        try container.encode(type, forKey: .type)
        try container.encode(updatedAt, forKey: .updatedAt)
        try container.encode(url, forKey: .url)
        try container.encode(hasBeenSeen, forKey: .hasBeenSeen)
    }

    override var description: String {
        """
        Name: \(name ?? "")
        Company: \(company ?? "")
        Blog: \(blog ?? "")
        Location: \(login ?? "")
        Email: \(email ?? "")
        Twitter: \(twitterUsername ?? "")
        """
    }
}

