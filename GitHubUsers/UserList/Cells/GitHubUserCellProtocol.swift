//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation


protocol GitHubUserCellProtocol {
    func setData(user: GitHubUser, index: Int)
    func onImageDataLoaded(data: Data)
    func setupLayout()
}
