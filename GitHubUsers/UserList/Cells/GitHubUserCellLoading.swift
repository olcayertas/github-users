//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit
import Combine

class GitHubUserCellLoading: UITableViewCell {


    let activityIndicator = UIActivityIndicatorView(style: .large)

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLayout()
    }

    func setupLayout() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        activityIndicator.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor).isActive = true
        activityIndicator.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
        activityIndicator.centerInParent(self)
        activityIndicator.startAnimating()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        activityIndicator.startAnimating()
    }
}