//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit
import Combine

class BaseGitHubUserCell: UITableViewCell, GitHubUserCellProtocol {

    internal let avatarClient = AvatarClient()
    internal var userNameLabel = UILabel()
    internal var detailsLabel =  UILabel()
    internal var avatarImageView = UIImageView()
    internal let mainStack = UIStackView()
    private var avatarSubscription: AnyCancellable?
    private var avatarImageData: Data?

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLayout()
    }

    func setupLayout() {
        selectionStyle = .none

        mainStack.axis = .horizontal
        mainStack.distribution = .fill
        mainStack.alignment = .center
        mainStack.spacing = 16
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mainStack)
        mainStack.anchors(equalTo: self)

        avatarImageView.image = UIImage(named: "user")?.withRenderingMode(.alwaysOriginal)
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        avatarImageView.layer.borderWidth = 3
        avatarImageView.layer.cornerRadius = 40
        avatarImageView.layer.borderColor = UIColor.black.cgColor
        avatarImageView.layer.masksToBounds = true
        mainStack.addArrangedSubview(avatarImageView)

        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false

        userNameLabel.font = UIFont(name: "Helvetica-Bold", size: 20)
        detailsLabel.font = UIFont(name: "Helvetica", size: 16)

        stack.addArrangedSubview(userNameLabel)
        stack.addArrangedSubview(detailsLabel)
        mainStack.addArrangedSubview(stack)
    }

    internal func addNoteIcon() {
        let noteImageView = UIImageView(image: UIImage(systemName: "note.text")!.withRenderingMode(.alwaysOriginal))
        noteImageView.contentMode = .scaleAspectFit
        noteImageView.tintColor = .black
        noteImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        mainStack.addArrangedSubview(noteImageView)
    }

    var user: GitHubUser?

    func setData(user: GitHubUser, index: Int) {
        self.user = user
        userNameLabel.text = "\(index) - \(user.login ?? "?")"
        detailsLabel.text = "Type: \(user.type ?? "?") - ID: \(user.id)"
        backgroundColor = user.hasBeenSeen ? .lightGray : .white
        loadAvatar(id: Int(user.id))
    }

    private func loadAvatar(id: Int) {
        avatarSubscription?.cancel()
        avatarSubscription = avatarClient.avatar(id: id).sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                return
                //print("Finished getting user(\(id)) avatar.")
            case .failure(let error):
                print("Failed to get user(\(id)) avatar: \(error.localizedDescription)")
            }
        }, receiveValue: { [weak self] data in
            self?.onImageDataLoaded(data: data)
        })
    }

    func onImageDataLoaded(data: Data) {
        avatarImageView.image = UIImage(data: data)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = UIImage(named: "user")?.withRenderingMode(.alwaysOriginal)
        userNameLabel.text = nil
        detailsLabel.text = nil
        backgroundColor = .white
        avatarSubscription?.cancel()
    }
}
