//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit

class GitHubUserCellWithNote: BaseGitHubUserCell {

    override func setupLayout() {
        super.setupLayout()
        addNoteIcon()
    }
}