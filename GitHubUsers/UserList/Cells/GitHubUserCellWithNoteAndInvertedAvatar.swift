//
// Created by Olcay Ertaş on 20.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit

class GitHubUserCellWithNoteAndInvertedAvatar: BaseGitHubUserCell {

    override func onImageDataLoaded(data: Data) {
        avatarImageView.image = UIImage(data: data)?.colorInverted().withRenderingMode(.alwaysOriginal)
    }

    override func setupLayout() {
        super.setupLayout()
        addNoteIcon()
    }
}