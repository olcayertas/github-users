//
// Created by Olcay Ertaş on 22.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit

extension GitHubUserListViewController {

    private func showLoadingIndicator() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(indicator)
        indicator.centerInParent(view)
        indicator.startAnimating()
    }

    private func hideLoadingIndicator() {
        indicator.startAnimating()
        indicator.removeFromSuperview()
    }

    internal func updateNoInterNetLabelHeight(_ isConnected: Bool) {
        noInternetLabel.removeConstraint(heightConstraint)
        heightConstraint = noInternetLabel.heightAnchor.constraint(
                equalToConstant: isConnected ? 0.001 : 40
        )
        heightConstraint.isActive = true
        UIView.animate(withDuration: 0.6) {
            self.view.layoutIfNeeded()
        }
    }

    internal func updateLoadingIndicatorState(_ loading: Bool) {
        loading ? showLoadingIndicator() : hideLoadingIndicator()
    }

    internal func setupLayout() {

        searchBar.searchTextField.textPublisher.sink { [weak self] text in
            self?.viewModel.searchText = text.lowercased()
        }.store(in: &bindings)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchBar)
        searchBar.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true

        noInternetLabel.translatesAutoresizingMaskIntoConstraints = false
        noInternetLabel.text = "NO INTERNET CONNECTION!"
        view.addSubview(noInternetLabel)
        heightConstraint = noInternetLabel.heightAnchor.constraint(equalToConstant: 0.001)
        heightConstraint.isActive = true
        noInternetLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noInternetLabel.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true

        dataSource = DataSource(tableView: tableView) { [weak self] view, path, user in
            if let identifier = self?.viewModel.cellIdentifierForItem(at: path.row) {
                let cell = view.dequeueReusableCell(withIdentifier: identifier, for: path)
                if let cell = cell as? GitHubUserCellProtocol, let user = self?.viewModel.users[path.row] {
                    cell.setData(user: user, index: path.row)
                }
                return cell
            } else {
                return UITableViewCell()
            }
        }

        var snapshot = NSDiffableDataSourceSnapshot<Section, GitHubUser>()
        snapshot.appendSections([.main])
        dataSource.apply(snapshot)

        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        view.addSubview(tableView)
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: noInternetLabel.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        viewModel.cellRegisterBlock = { [weak self] aClass, identifier in
            self?.tableView.register(aClass, forCellReuseIdentifier: identifier)
        }
    }
}
