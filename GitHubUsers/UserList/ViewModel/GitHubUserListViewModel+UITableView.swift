//
// Created by Olcay Ertaş on 22.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import UIKit


extension GitHubUserListViewModel {

    func cellIdentifierForItem(at index: Int) -> String {
        if loadingMore, index == users.count - 1 {
            return String(describing: GitHubUserCellLoading.self)
        }
        let isInverted = index > 0 && (index + 1) % 4 == 0
        let hasNote = users[index].note != nil && users[index].note != ""
        if isInverted && hasNote {
            return String(describing: GitHubUserCellWithNoteAndInvertedAvatar.self)
        } else if isInverted {
            return String(describing: GitHubUserCellWithInvertedAvatar.self)
        } else if hasNote {
            return String(describing: GitHubUserCellWithNote.self)
        } else {
            return String(describing: GitHubUserCellDefault.self)
        }
    }

    func loadMoreIfNeeded(index: Int) {
        // If we are already reloading ignore
        if loadingMore {
            return
        }
        // We are not searching and we are reaching the end of the list.
        if searchText.isEmpty && ((users.count - index) < 10) {
            print("loading more....")
            loadingMore = true
            //Add a dummy user to drive the cell count
            let user = GitHubUser(context: GHUContainer.instance.backgroundContext)
            user.id = Int64(UUID().hashValue)
            user.login = UUID().uuidString
            users.append(user)
            appendingUsers = [user]
            // We are delaying fetch on purpose to make it visible that
            // we are showing an activity indicator cell at the end of the list.
            // Must be removed from production code.
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
                self?.getUsers()
            }
        }
    }

    public func refresh() {
        // We are delaying fetch on purpose for better UX.
        // Refresh control is hiding to fast some times.
        // In this case users is not sure whether he/she was able to do it.
        refreshing = true
        loadingMore = false
        loading = false
        since = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.getUsers()
        }
    }

    func registerCells() {
        if let moduleName = Bundle.main.infoDictionary!["CFBundleName"] as? String {
            identifiers.forEach { identifier in
                if let aClass = NSClassFromString(moduleName + "." + identifier) {
                    cellRegisterBlock?(aClass, identifier)
                }
            }
        }
    }
}

extension GitHubUserListViewController: UITableViewDelegate {

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier = "GitHubUserDetailViewController"
        if let controller: GitHubUserDetailViewController = UIStoryboard.viewController(identifier) {
            let user = viewModel.users[indexPath.row]
            controller.user = user
            controller.user?.hasBeenSeen = true
            try? user.managedObjectContext?.save()
            if let cell = tableView.cellForRow(at: indexPath) as? GitHubUserCellProtocol {
                cell.setData(user: viewModel.users[indexPath.row], index: indexPath.row)
            }
            lastUserIndex = indexPath.row
            navigationController?.pushViewController(controller, animated: true)
        }
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.loadMoreIfNeeded(index: indexPath.row)
    }
}
