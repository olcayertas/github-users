//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import CoreData

typealias GitHubUserRequest = NSFetchRequest<GitHubUser>

class GitHubUserListViewModel: ObservableObject {

    internal var actualUsers = [GitHubUser]()
    internal var since = 0
    internal var fetchLimit = UserDefaults.standard.integer(forKey: "fetchLimit") {
        didSet {
            UserDefaults.standard.setValue(fetchLimit, forKey: "fetchLimit")
        }
    }

    private let gitHubClient = GitHubUserListClient()
    private let reachability = ReachabilityService()
    private var cancelBag = Set<AnyCancellable>()

    internal let identifiers = [
        "GitHubUserCellLoading",
        "GitHubUserCellDefault",
        "GitHubUserCellWithNote",
        "GitHubUserCellWithInvertedAvatar",
        "GitHubUserCellWithNoteAndInvertedAvatar"
    ]

    @Published var users = [GitHubUser]()
    @Published var appendingUsers = [GitHubUser]()
    @Published var deletingUsers = [GitHubUser]()
    @Published var refreshing = false
    @Published var loadingMore = false
    @Published var loading = false
    @Published var isSearching = false
    // Must be true by default
    @Published var isConnected = true
    // First one for insertions and the second one is for removals,

    init() {
        //print("GitHubUserListViewModel: init()")
        gitHubClient.decoder.userInfo[.managedObjectContext] = GHUContainer.instance.backgroundContext
        reachability.$isNetworkAvailable.sink { [weak self] isConnected in
            let value = isConnected ?? false
            self?.isConnected = value
        }.store(in: &cancelBag)
    }

    var searchText = "" {
        didSet {
            //print("Search text: \(searchText)")
            filter()
        }
    }

    var cellRegisterBlock: ((AnyClass, String) -> ())? {
        didSet {
            registerCells()
        }
    }

    func getUsers() {
        loadingMore ? print("getUsers - loadingMore") : print("getUsers")
        getUsersFromCoreData {
            getUsersFromNetwork()
        }
    }

    func getUsersFromNetwork() {
        print("getUsersFromNetwork since \(since) - user count is  \(users.count)")
        gitHubClient.users(since: since).sink(receiveCompletion: { [weak self] completion in
            switch completion {
            case .finished:
                return
                    //print("Finished getting users.")
            case .failure(let error):
                self?.refreshing = false
                self?.loading = false
                self?.loadingMore = false
                print("Failed to get users: \(error.localizedDescription)")
            }
        }, receiveValue: { [weak self] users in
            self?.onReceiveUsers(users)
        }).store(in: &cancelBag)
    }

    private func onReceiveUsers(_ remoteUsers: [GitHubUser]) {
        print("received \(remoteUsers.count) users from network")

        remoteUsers.forEach { user in
            try? user.managedObjectContext?.save()
        }

        fetchLimit = remoteUsers.count
        since = Int(remoteUsers.last?.id ?? 0)
        print("Since is now \(since)")

        if let id1 = actualUsers.last?.id,
           let id2 = remoteUsers.last?.id,
           id1 != id2 {
            // these are new values, we will append

            // This condition will only be true for first run
            if loadingMore  {
                deletingUsers = [users.removeLast()]
            }
            actualUsers.append(contentsOf: remoteUsers)
            users.append(contentsOf: remoteUsers)
            appendingUsers = remoteUsers
        }
        else if actualUsers.isEmpty == true {
            //This is the firs fetch result
            actualUsers.append(contentsOf: remoteUsers)
            users.append(contentsOf: remoteUsers)
            appendingUsers = remoteUsers
        }
        else {
            //We have users from database already.
        }

        print("User count is now \(users.count)")

        loading = false
        loadingMore = false
        refreshing = false

        //GHUContainer.instance.saveContext()
        //try? GHUContainer.instance.backgroundContext.save()
    }
}
