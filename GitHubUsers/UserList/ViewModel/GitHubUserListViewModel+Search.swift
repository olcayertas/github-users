//
// Created by Olcay Ertaş on 22.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

extension GitHubUserListViewModel {

    internal func filter() {
        isSearching = !searchText.isEmpty
        if searchText.isEmpty {
            users = actualUsers
        } else {
            let filtered = actualUsers.filter { user in
                //print("Search for \(user.login ?? "")")
                user.login?.localizedCaseInsensitiveContains(searchText) == true ||
                        user.note?.localizedCaseInsensitiveContains(searchText) == true
            }
            users = filtered
        }
    }
}