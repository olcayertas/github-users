//
// Created by Olcay Ertaş on 22.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import CoreData

extension GitHubUserListViewModel {

    internal func clearCoreData() {
        GHUContainer.instance.clear()
    }

    internal func getUsersFromCoreData(_ completion: () -> ()) {
        print("getUsersFromCoreData since \(since) - user count is  \(users.count)")
        loading = users.count == 0

        do {
            let request = GitHubUser.fetchRequest(since, fetchLimit)
            let response = try GHUContainer.instance.viewContext.fetch(request)
            print("Fetched \(response.count) users from Core Data")
            if loadingMore {
                // If we have users in database hide loading cell
                if response.count > 0 {
                    deletingUsers = [users.removeLast()]
                }
                actualUsers.append(contentsOf: response)
                users.append(contentsOf: response)
                appendingUsers = response
            } else if refreshing {
                actualUsers = response
                users = response
                appendingUsers = response
            } else {
                //This is first load
                actualUsers = response
                users = response
                appendingUsers = response
            }
            // Notify to show core data result
            completion()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}