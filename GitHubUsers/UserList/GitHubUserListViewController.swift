//
//  ViewController.swift
//  GitHubUsers
//
//  Created by Olcay Ertaş on 19.12.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import UIKit
import Combine

class GitHubUserListViewController: UIViewController {
    
    enum Section {
        case main
    }
    
    typealias DataSource = UITableViewDiffableDataSource<Section, GitHubUser>

    internal let viewModel = GitHubUserListViewModel()
    internal let tableView = UITableView()
    internal let searchBar = UISearchBar()
    internal var bindings = Set<AnyCancellable>()
    internal let indicator = UIActivityIndicatorView(style: .large)
    internal let noInternetLabel = UILabel()
    internal var heightConstraint: NSLayoutConstraint!
    internal let refreshControl = UIRefreshControl()
    internal var dataSource: DataSource!
    internal var lastUserIndex: Int?

    private var clearDB = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupBindings()
        navigationController?.navigationBar.isHidden = true
        // Only for first time or pull to refresh. Others are load more.
        clearDB ? viewModel.clearCoreData() : viewModel.getUsers()
        if let index = lastUserIndex {
            viewModel.users[index].note?.print()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let index = lastUserIndex {
            //viewModel.users[index].note?.print()
            var snapshot = dataSource.snapshot()
            snapshot.reloadItems([viewModel.users[index]])
            dataSource.apply(snapshot)
        }
    }

    private func setupBindings() {
        // For internet connectivity check
        viewModel.$isConnected.sink { [weak self] isConnected in
            //print("isConnected: \(isConnected)")
            DispatchQueue.main.async {
                self?.updateNoInterNetLabelHeight(isConnected)
            }
        }.store(in: &bindings)
        // For animating insertions
        viewModel.$appendingUsers.sink { [weak self] users in
            DispatchQueue.main.async {
                self?.tableView.refreshControl?.endRefreshing()
                self?.appendUsers(users)
            }
        }.store(in: &bindings)
        viewModel.$deletingUsers.sink { [weak self] users in
            DispatchQueue.main.async {
                self?.deleteUsers(users)
            }
        }.store(in: &bindings)
        // For showing loading indicator
        viewModel.$loading.sink { [weak self] loading in
            DispatchQueue.main.async {
                self?.updateLoadingIndicatorState(loading)
            }
        }.store(in: &bindings)
        // For showing loading indicator
        viewModel.$isSearching.sink { [weak self] isSearching in
            DispatchQueue.main.async {
                self?.tableView.refreshControl = isSearching ? nil : self?.refreshControl
            }
        }.store(in: &bindings)
    }

    private func appendUsers(_ users: [GitHubUser]) {
        tableView.refreshControl?.endRefreshing()
        var snapshot = dataSource.snapshot()
        snapshot.appendItems(users)
        dataSource.apply(snapshot)
    }

    private func deleteUsers(_ users: [GitHubUser]) {
        tableView.refreshControl?.endRefreshing()
        var snapshot = dataSource.snapshot()
        snapshot.deleteItems(users)
        dataSource.apply(snapshot)
    }

    @objc internal func refresh() {
        viewModel.refresh()
    }
}

