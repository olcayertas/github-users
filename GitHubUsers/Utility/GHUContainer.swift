//
// Created by Olcay Ertaş on 23.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import CoreData


class GHUContainer {

    private var container = NSPersistentContainer(name: "GitHubUser")

    var backgroundContext: NSManagedObjectContext!
    var viewContext: NSManagedObjectContext!

    static let instance = GHUContainer()

    private init() {
        func onload() {
            viewContext = container.viewContext
            backgroundContext = container.newBackgroundContext()
            backgroundContext.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
        }
        container.loadPersistentStores { storeDescription, error in
            if let error = error {
                print("Unresolved error \(error)")
            } else {
                onload()
            }
        }
    }

    func saveContext() {
        if container.viewContext.hasChanges {
            do {
                try container.viewContext.save()
            } catch {
                print("An error occurred while saving: \(error)")
            }
        }
    }

    func clear() {
        print("clearCoreData")
        let request: GitHubUserRequest = GitHubUser.fetchRequest()
        request.includesPropertyValues = false
        do {
            try backgroundContext.fetch(request).forEach { user in
                backgroundContext.delete(user)
            }
            saveContext()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
