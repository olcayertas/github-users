//
// Created by Olcay Ertaş on 19.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

class GitHubUserDecoder: JSONDecoder {

    override init() {
        super.init()
        keyDecodingStrategy = .convertFromSnakeCase
    }
}
