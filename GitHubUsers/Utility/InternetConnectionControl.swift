//
// Created by Olcay Ertaş on 21.12.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Combine
import Network

enum NetworkType {
    case wifi
    case cellular
    case loopBack
    case wired
    case other
}

final class ReachabilityService: ObservableObject {

    private var path: NWPath?
    private var typeOfCurrentConnection: NetworkType?
    private let monitor = NWPathMonitor()
    private let backgroundQueue = DispatchQueue.global(qos: .background)

    @Published var isNetworkAvailable: Bool?

    init() {
        setUp()
    }

    init(with interFaceType: NWInterface.InterfaceType) {
        setUp()
    }

    deinit {
        monitor.cancel()
    }
}

private extension ReachabilityService {

    func setUp() {

        func handlePath(_ path: NWPath) {
            self.path = path
            switch path.status {
            case .satisfied:
                //print("ReachabilityService: satisfied")
                isNetworkAvailable = true
            case .unsatisfied:
                //print("ReachabilityService: unsatisfied")
                if #available(iOS 14.2, *) {
                    switch path.unsatisfiedReason {
                    case .notAvailable:
                        //print("ReachabilityService: unsatisfiedReason: notAvailable")
                        break
                    case .cellularDenied:
                        //print("ReachabilityService: unsatisfiedReason: cellularDenied")
                        break
                    case .wifiDenied:
                        //print("ReachabilityService: unsatisfiedReason: wifiDenied")
                        break
                    case .localNetworkDenied:
                        //print("ReachabilityService: unsatisfiedReason: localNetworkDenied")
                        break
                    @unknown default:
                        //print("ReachabilityService: unsatisfiedReason: default")
                        break
                    }
                }
                isNetworkAvailable = false
            case .requiresConnection:
                //print("ReachabilityService: requiresConnection")
                isNetworkAvailable = false
            @unknown default:
                //print("ReachabilityService: default")
                isNetworkAvailable = true
            }
            if path.usesInterfaceType(.wifi) {
                typeOfCurrentConnection = .wifi
            } else if path.usesInterfaceType(.cellular) {
                typeOfCurrentConnection = .cellular
            } else if path.usesInterfaceType(.loopback) {
                typeOfCurrentConnection = .loopBack
            } else if path.usesInterfaceType(.wiredEthernet) {
                typeOfCurrentConnection = .wired
            } else if path.usesInterfaceType(.other) {
                typeOfCurrentConnection = .other
            }
        }
        monitor.pathUpdateHandler = { path in
            handlePath(path)
        }
        monitor.start(queue: backgroundQueue)
    }
}